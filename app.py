from flask import Flask
import json

app = Flask(__name__)

@app.route('/')
def home():
    return 'Hello, World!'

@app.route('/posts')
def getPostsImages():
    posts = [
      {
        'postID':1,
        'image':"https://www.recipetineats.com/wp-content/uploads/2019/03/Oven-Baked-Chicken-Breast_0-SQ.jpg"
      },
      {
        'postID':2,
        'image':"https://www.gohawaii.com/sites/default/files/hero-unit-images/11500_mauibeaches.jpg"
      }
    ]
    json_string = json.dumps(posts)
    return json_string

if __name__ == '__main__':
    app.run(debug=True, port=8000)